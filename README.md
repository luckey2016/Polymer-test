# Polymer单元测试简介

Polymer单元测试工具的底层库称为Web Component Tester
它是建立在第三方工具之上，包括：
Mocha测试框架，支持BDD和TDD。
Chai 用于Mocha测试的更多断言类型
Sinon 用于检查，调试和模拟
Selenium 用于针对多个浏览器运行测试
Accessibility Developer Tools 用于辅助功能审核


## 测试启动步骤
1.polymer serve
2.将运行路径改为test目录




## 一.异步测试

通常用于内部事件传递到外部

demo-1.html

```
_doEvent() {
	this.dispatchEvent(new CustomEvent(‘test’, {detail: ‘testing!’}));
}
```

demo-1_test.html

```
test(‘测试demo-1的_doEvent方法事件’, function(done) {
	myEl.addEventListener(‘test’,function(event) {
		assert.equal(event.detail, ‘tested!’);
		done();
	});
	myEl.fireEvent();
});
```

创建一个异步测试，先将done参数传递给测试函数，然后done()在测试完成时调用。这个done是给mocha的一个信号。当Mocha运行测试时，它将等待直到调用done()方法回调。如果done()未调用回调，则测试最终超时，Mocha将测试报告为失败，即测试未通过。


## 二.使用测试夹具

1.定义测试夹具模版并给它一个ID
2.在测试脚本中定义一个变量来引用该模版
3.在setup()方法中实例化一个新的对象

通常用于组件中传参的测试


```
<test-fixture id="my-el-fixture">
  <template>
    <my-el prop1="test value">
    </my-el>
  </template>
</test-fixture>

<script>
  suite('<my-el>', function() {
    var myEl;
    setup(function() {
      myEl = fixture('my-el-fixture');
    });
    test('sets the "prop1" property from markup', function() {
      assert.equal(myEl.prop1, 'test value');
    });
  });
</script>
```

## 三.创建stub方法

有助于使用自定义方法替换默认实现，对于捕捉异常很有用


```
setup(function() {
  stub('paper-button', {
    click: function() {
      console.log('paper-button.click called');
    }
  });
});
```



## 四.创建stub elements

使用stub elements来替换测试元素。例如，如果你的一个测试元素依赖于另一个返回数据的元素，而不是将其他(可能不稳定的)元素导入到测试中，则可以实现总是返回同一元素的相同数据
使用replace()方法来创建stub elements


```
setup(function() {
  replace('paper-button').with('fake-paper-button');
});
```

*尽可能通过元素的ID去使用该元素而非querySelector(‘paper-button’)的形式


## 五.AJAX

Web组件测试器包括Sinon，这可以帮助模拟XHR请求并创建假服务器
下面是一个简单的XHR单元测试套件的例子，并使用了Chai的expect断言风格：


```
<!-- create test fixture template -->
<test-fixture id="simple-get">
  <template>
    <iron-ajax url="/responds_to_get_with_json"></iron-ajax>
  </template>
</test-fixture>
<script>
  suite('<iron-ajax>', function() {
    var ajax;
    var request;
    var server;
    var responseHeaders = {
      json: { 'Content-Type': 'application/json' }
    };
    setup(function() {
      server = sinon.fakeServer.create();
      server.respondWith(
        'GET',
        /\/responds_to_get_with_json.*/, [
          200,
          responseHeaders.json,
          '{"success":true}'
        ]
      );
    });
    teardown(function() {
      server.restore();
    });
    suite('when making simple GET requests for JSON', function() {
      setup(function() {
        // get fresh instance of iron-ajax before every test
        ajax = fixture('simple-get');
      });
      test('has sane defaults that love you', function() {
        request = ajax.generateRequest();
        server.respond();
        expect(request.response).to.be.ok;
        expect(request.response).to.be.an('object');
        expect(request.response.success).to.be.equal(true);
      });
      test('has the correct xhr method', function() {
        request = ajax.generateRequest();
        expect(request.xhr.method).to.be.equal('GET');
      });
    });
  });
</script>
```


## 六.运行一套测试

指的是运行多个html的测试，创建一个HTML文件并调用loadSuites()方法
例：


```
<!doctype html>
<html>
  <head>
    <meta charset="utf-8">
    <script src=”../bower_components/webcomponentsjs/webcomponents-lite.js”></script>
    <script src=”../bower_components/web-component-tester/browser.js”></script>
  </head>
  <body>
    <script>
      WCT.loadSuites([
        'basic.html',
        'async.html'
      ]);
    </script>
  </body>
</html>
```

## 七.混合测试：测试本地DOM

对于混合元素，使用Polymer的DOM API来访问和修改本地DOM子项。

```
test('click sets isWaiting to true', function() {
  myEl.$$('button').click();
  assert(myEl.isWaiting, true);
});

```

*其中myEL.$$(‘button’)返回的是在本地DOM中遇到的第一个元素

测试DOM突变
如果模版包含dom-repear或dom-if，或者涉及Shadow DOM突变，可以使用flush，flush确保发生异步更改。


```
suite('my-list tests', function() {
  var list, listItems;
  setup(function() {
    list = fixture('basic');
  });
  test('Item lengths should be equal', function(done) {
    list.items = [
      'Responsive Web App boilerplate',
      'Unit testing with Web Component Tester',
      'Offline support with the Platinum Service Worker Elements'
    ];
    // Data bindings will stamp out new DOM asynchronously
    // so wait to check for updates
    flush(function() {
      listItems = list.shadowRoot.querySelectorAll('li');
      assert.equal(list.items.length, listItems.length);
      done();
    });
  });
)};

```




# 更多类型的单元测试实例


测试对象：

```
<core-selector id=“selector1”>
  <div id=“item1”>Item 1</div>
  <div id=“item2”>Item 2</div>
  <div id=“item3”>Item 3</div>
</core-selector>


var s = document.querySelector(‘#selector1’);
```

1.测试默认情况下没有选择
```
assert.equal(s.selected, null);
```


2.测试属性是否是我们预期的支持多选
```
assert.isFalse(s.multi);
```


3.测试选项数量是否与我们预期的数量一致
```
assert.equal(s.items.length, 3);
```

4.通过在此元素上设置自定义属性来覆盖此类，来测试确保设置了正确的类(默认)
```
assert.equal(s.selectedClass, ‘core-selected’);
```

5.测试点击事件触发的选中事件
```
assert.equal(s.selectedClass, ‘core-selected’);
var selectEventCounter = 0;
s.addEventListener(‘core-select’, function(e){
	if (e.detail.isSelected){
        selectEventCounter++;
        assert.equal(e.detail.item, s.selectedItem);
      }
});
```

6.测试选中任意一个项目
```
s.selected = ‘item2’;
```

在Polymer的单元测试中，为了确保当我们以这种方式动态更改数值时，我们所有的绑定都能够被正确地更新，我们可以调用Platform.flush()

```
Platform.flush();
```




7.为了检查已经设置的值和所选的类和项目值如预期的那样，我们也可以使用简单的超时的方式来模拟
```
setTimeout(function(){
      // check core-select event
	assert.equal(selectedEventCounter, 1);

	// check selected class
	assert.isTrue(s.children[1].classList.contains(‘my-selected’));

	// check selectedItem
	assert.equal(s.selectedItem, s.children[1]);
}, 50);
```





Your application is already set up to be tested via [web-component-tester](https://github.com/Polymer/web-component-tester). Run `polymer serve --open` to run your application's test suite locally.
















